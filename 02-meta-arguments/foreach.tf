resource "aws_vpc" "for_each_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    name = "kenny-vpc"
     
  }

}


#######################################
# FOR EACH FOR PUBLIC SUBNET USING LOCAL
######################################
locals {
  public_subnet ={
      pub_subnet_1 = {
       cidr_block = "10.0.0.0/24" 
       az = "us-east-1a"  
      }
     pub_subnet_2 = {
       cidr_block = "10.0.2.0/24" 
       az = "us-east-1b"  
      }
  }

}





resource "aws_subnet" "public_subnet_for_each" {
  for_each = local.public_subnet

  vpc_id     = local.vpc_id
  cidr_block = each.value.cidr_block
  availability_zone = each.value.az
  map_public_ip_on_launch = true

  tags = {
    Name = "each.key"
  }
}



################################################3
# FOR EACH FOR PUBLIC SUBNET USING VARIABLE
##################################################

# resource "aws_subnet" "public_subnet_for_each" {
#   for_each = var.public_subnet

#   vpc_id     = local.vpc_id
#   cidr_block = each.value.cidr_block
#   availability_zone = each.value.az
#   map_public_ip_on_launch = true

#   tags = {
#     Name = "each.key"
#   }
# }

# variable "public_subnet" {
#     # type = map(any)
#     # description = "onjects of subnet to be crated"
#     default = {
#       public_subnet_1 = {
#           cidr_block = "10.0.0.0/24"
#           az = "us-east-1a"
#       }

#        public_subnet_2 = {
#           cidr_block = "10.0.2.0/24"
#           az = "us-east-1b"
#       }
#     }
    
# }
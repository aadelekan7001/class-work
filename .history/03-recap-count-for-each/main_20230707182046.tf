// create 2 instances making us of count and data source to pull down a key pair and ami



resource "aws_vpc" "kenny" {
  cidr_block = var.vpc_cidr
  tags = {
    name = "kenny-vpc"
     
  }
}



resource "aws_instance" "web" {
    count = 2
  ami           = data.aws_ami.ami.id
  instance_type = "t3.micro"
  //key_name = var.key_name //use data source to pull

  tags = {
    Name = "HelloWorld"
  }
}
data "aws_ami" "ami" {
  
  most_recent      = true
  
  owners           = ["amazon"]

  filter {
    name   = "name"
    values = ["myami-*"] //al2023-ami-2023.1.20230705.0-kernel-6.1-x86_64
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}
// create 2 instances making us of count and data source to pull down a key pair and ami



resource "aws_vpc" "kenny" {
  cidr_block = "10.0.0.0/16"
  tags = {
    name = "kenny-vpc"
     
  }
}

resource "aws_subnet" "public_subnet_1" {

  vpc_id     = aws_vpc.kenny.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  

  tags = {
    Name = "subnet-1"
  }
}


resource "aws_subnet" "public_subnet_2" {

  vpc_id     = aws_vpc.kenny.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-1b"
  

  tags = {
    Name = "subnet-2"
  }
}



resource "aws_instance" "web" {
    count = 2
  ami           = data.aws_ami.ami.id
  instance_type = "t3.micro"
  key_name = var.key_name //use data source to pull
  subnet_id = [aws_subnet.public_subnet_1.id,aws_subnet.public_subnet_2.id][count.index]
  tags = {
    Name = "app-${count.index + 1}"
  }
}
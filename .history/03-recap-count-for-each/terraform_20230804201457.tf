terraform {
    required_version = ">=1.3.0"

backend "s3" {
  bucket = "tayo4658737"
  key = "terraform/env/prod"
  region = "us-east-1"
  dynamodb_table = "value"
}



  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.55.0"
    }
  }
}
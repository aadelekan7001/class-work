// create 2 instances making us of count and data source to pull down a key pair and ami

resource "aws_instance" "web" {
  ami           = "ami-06ca3ca175f37dd66"
  instance_type = "t3.micro"
  key_name = var.key_name //use data source to pull

  tags = {
    Name = "HelloWorld"
  }
}
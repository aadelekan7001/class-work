data "aws_ami" "ami" {
  
  most_recent      = true
  
  owners           = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-*kernel-6.1-x86_64"] //al2023-ami-2023.1.20230705.0-kernel-6.1-x86_64
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

data "aws_key_pair" "example" {
  key_name           = "test"
  include_public_key = true

  filter {
    name   = "tag:Component"
    values = ["web"]
  }
}
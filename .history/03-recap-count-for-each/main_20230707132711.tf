#########################
# create 2 instances using count and for each - use data source to pull down keypair
########################

resource "aws_instance" "web" {
    count = 2
  ami           = "ami-06ca3ca175f37dd66"
  instance_type = "t3.micro"
  key_name = "add-key"

  tags = {
    Name = "HelloWorld"
  }
}
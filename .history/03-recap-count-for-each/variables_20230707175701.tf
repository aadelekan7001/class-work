variable "key_name" {
  type = string
  description = "name of the key pair to use"
  default = "joyy"
}
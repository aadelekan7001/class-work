// create 2 instances making us of count and data source to pull down a key pair and ami

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  key_name = "" //use data source to pull

  tags = {
    Name = "HelloWorld"
  }
}
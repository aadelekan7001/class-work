# used to pull down existing resources in AWS
# 


# ami
# instance type
# key_pair
# role
# security group
# storage
# aws_subnet
# availability_zone
data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_ami" "ami" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-*-kernel-6.1-x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

   
}
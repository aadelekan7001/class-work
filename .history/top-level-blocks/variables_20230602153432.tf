// variable block
// used to avoid hardcoding
//

variable "subnet_1_cidr" {
  type = string
  description = "value of subnet cidr"
  default = "10.0.1.0/24"
}


variable "subnet_2_cidr" {
  type = string
  description = "value of subnet cidr"
  default = "10.0.2.0/24"
}


variable "vpc_cidr" {
  type = string
  description = "value of vpc cidr"
  default = "10.0.0.0/16"
}

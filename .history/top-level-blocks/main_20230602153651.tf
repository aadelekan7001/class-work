


resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
  tags = {
    name = "kenny-vpc"
     
  }

}

resource "aws_subnet" "subnet_1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.subnet_1_cidr
  availability_zone = "us-east-1a"

  tags = {
    Name = "Main"
  }
}


resource "aws_subnet" "subnet_2" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.subnet_2_cidr
  availability_zone = "us-east-1b"

  tags = {
    Name = "Main"
  }
}
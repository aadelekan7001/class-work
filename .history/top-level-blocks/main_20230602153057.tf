


resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
  tags = {
    name = "kenny-vpc"
     
  }

}

resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.subnet_cidr
  availability_zone = "us-east-1a"

  tags = {
    Name = "Main"
  }
}
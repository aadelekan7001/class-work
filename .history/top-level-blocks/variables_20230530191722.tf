// variable block
// used to avoid hardcoding

variable "subnet_cidr" {
  type = string
  description = "value of subnet cidr"
  default = "10.0.1.0/24"
}
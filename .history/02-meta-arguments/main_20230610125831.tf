


resource "aws_vpc" "kenny" {
  cidr_block = var.vpc_cidr
  tags = {
    name = "kenny-vpc"
     
  }

}

resource "aws_subnet" "public_subnets" {
  count = length(var.public_subnet_cidr)
  vpc_id     = local.vpc_id
  cidr_block = var.public_subnet_cidr/*
  availability_zone = "us-east-1a"

  tags = {
    Name = "Main"
  }
}


# # resource "aws_subnet" "private_subnet_2" {
# #   vpc_id     = local.vpc_id
# #   cidr_block = var.subnet_2_cidr
# #   availability_zone = data.aws_availability_zones.available.names[1]

# #   tags = {
# #     Name = "Main"
# #   }
# # }


# # resource "aws_subnet" "public_subnet_1" {
# #   vpc_id     = local.vpc_id
# #   cidr_block = var.subnet_2_cidr
# #   availability_zone = local.az_1

# #   tags = {
# #     Name = "Main"
# #   }
# # }


# resource "aws_instance" "web" {
#   ami           = data.aws_ami.ami.id
#   instance_type = "t2.micro"

#   tags = {
#     Name = "HelloWorld"
#   }
# }

###################
# create a keypair on the console
# use data source to pull down that keypair
# reference on an instance to create key pair
##################
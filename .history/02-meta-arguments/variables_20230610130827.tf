

variable "public_subnet_cidr" {
  type = list(any)
  description = "value of public subnet cidr"
  default = ["10.0.0.0/24", "10.0.2.0/24","10.0.6.0/24", "10.0.4.0/24"]
}


variable "vpc_cidr" {
  type = string
  description = "value of vpc cidr"
  default = "10.0.0.0/16"
}

// variable block
// used to avoid hardcoding
//

variable "public_subnet_cidr" {
  type = list(any)
  description = "value for public subnets"

}


variable "subnet_2_cidr" {
  type = string
  description = "value of subnet cidr"
  default = "10.0.2.0/24"
}


variable "vpc_cidr" {
  type = string
  description = "value of vpc cidr"
  default = "10.0.0.0/16"
}

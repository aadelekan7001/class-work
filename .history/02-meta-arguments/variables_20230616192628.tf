// variable block
// used to avoid hardcoding
//

variable "public_subnet_cidr" {
  type = list(any)
  description = "value for public subnets"
  default = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]

}



// prevent redundancy - make your code more readable
// if the same value appears more than one

locals {
  vpc_id = aws_vpc.for_each_vpc.id
}

locals {
  az = data.aws_availability_zones.available.names
  ty = [aws_subnet.public_subnet_1.id,aws_subnet.public_subnet_2.id]
}
resource "aws_vpc" "for_each_vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    name = "kenny-vpc"
     
  }

}


####################################
# FOR EACH FOR PUBLIC SUBNET USING LOCAL
###################################

resource "aws_subnet" "public_subnet_for_each" {
  count = length(var.public_subnet_cidr)

  vpc_id     = local.vpc_id
  cidr_block = var.public_subnet_cidr[count.index]
  availability_zone = local.az[count.index] // ToDO (element)
  map_public_ip_on_launch = true

  tags = {
    Name = "public_subnets-${count.index + 1}"
  }
}
resource "aws_s3_bucket" "example" {
  bucket = "my-tf-test-bucket"

lifecycle {
  prevent_destroy = true
}

}
// create DYNAMO DB
resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "terraform-locking-db"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "UserId"
  

  attribute {
    name = "UserId"
    type = "S"
  }


lifecycle {
  prevent_destroy = true
}
}
variable "bucket_name" {
  description = "name of the s3 bucket"
  default = "s3_bucket"
}
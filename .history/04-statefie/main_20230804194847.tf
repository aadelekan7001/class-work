resource "aws_s3_bucket" "example" {
  bucket = "my-tf-test-bucket"

lifecycle {
  prevent_destroy = true
}

}
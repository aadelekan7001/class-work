resource "aws_s3_bucket" "this" {
    count = length(var.bucket_name)
  bucket = "tayo4658737"

lifecycle {
  prevent_destroy = true
}

}
// create DYNAMO DB
resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "terraform-locking-db"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID" #IMPORTANT
  

  attribute {
    name = "LockID"
    type = "S"
  }


lifecycle {
  prevent_destroy = true
}
}